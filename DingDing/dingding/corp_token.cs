﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DingDing.dingding
{
    public class Corp_token
    {
        string _corp_token;
        string _expires_in;

        /// <summary>  
        /// 获取到的凭证
        /// </summary>  
        public string corp_token
        {
            get { return _corp_token; }
            set { _corp_token = value; }
        }

        /// <summary>  
        /// 凭证有效时间，单位：秒  
        /// </summary>  
        public string expires_in
        {
            get { return _expires_in; }
            set { _expires_in = value; }
        }

    }
}
