﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DingDing.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DingDing.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class DingdingController : ControllerBase
    {
        // GET: api/Dingding
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Dingding/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Dingding
        [HttpPost]
        public void Post([FromBody] string value)
        {
            LyCommon.WriteLog("执行了dingding:Post");
        }

        // PUT: api/Dingding/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
