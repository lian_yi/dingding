﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace DingDing.Common
{
    public class LyCommon
    {
        //public DateTime dt1970 = new DateTime(1970, 1, 1, 0, 0, 0, 0);

        public static void WriteLog(string strMemo, string applogsLogTxt = "Applogs\\log.txt")
        {
            string path = System.AppContext.BaseDirectory;
            //string path = Path.GetDirectoryName(typeof(Program).Assembly.Location);

            string filename = path + applogsLogTxt;
            if (!Directory.Exists(path + "\\Applogs\\"))
                Directory.CreateDirectory(path + "\\Applogs\\");
            StreamWriter sr = null;
            try
            {
                if (!File.Exists(filename))
                {
                    sr = File.CreateText(filename);
                }
                else
                {
                    sr = File.AppendText(filename);
                }
                sr.WriteLine(DateTime.Now + "\r\n" + strMemo + "\r\n");
            }
            catch
            {
            }
            finally
            {
                if (sr != null)
                    sr.Close();
            }
        }
        /// <summary>
        /// 得到时间戳
        /// </summary>
        /// <param name="dateTime">当前时间（UTC）</param>
        /// <returns></returns>
        public static long GetTimestamp(DateTime dateTime)
        {
            return (dateTime.Ticks - new DateTime(1970, 1, 1, 0, 0, 0, 0).Ticks) / 10000;
        }

    }
}
